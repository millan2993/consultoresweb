# Consultoresweb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Notas Pruebas

* El IDE usado fue: PhpStorm, ya que este normalmente lo uso para PHP y Angular.

* El servicio web no respondía correctemente, por lo tanto avancé la aplicación lo más que pude pero sin realizarle pruebas de su correcto funcionamiento, sin embargo, se intentó implementar todos los requerimientos y consumo del REST. 

    El error que mostraba es por los CORS, que normalmente se corrige desde el backend, al cual no tengo acceso.  

* El repositorio Git compartido no estaba accesible. Por ello he compartido este prouyecto en GitLab dejándolo público para su acceso. 

* Se hizo uso de: rutas, peticiones http, observables, componentes, Bootstrap.


Me hubiera gustado tener la oportunidad de probar la aplicación funcionando, aunque este método también nos permite demostrar el conocimiento del Framework sin necesidad de validar a prueba error.

Cualquier duda o inquietud, con mucho gusto.
