export interface MunicipioJurisdicionCamara {
  Pais: string;
  Dpto: string;
  Mpio: string;
  DescripcionMunicipio: string;
}
