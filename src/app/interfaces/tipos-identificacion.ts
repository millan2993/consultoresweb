export interface TiposIdentificacion {
  TpId: string;
  TpId_Desc: string;
  Operacion: string;
  RecordId: string;
  r_order: string;
  crr: string;
}
