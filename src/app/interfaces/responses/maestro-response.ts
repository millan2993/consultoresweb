import {Municipio} from '../municipio';
import {Pais} from '../pais';
import {TiposIdentificacion} from '../tipos-identificacion';
import {TipoVia} from '../tipo-via';
import {MunicipioJurisdicionCamara} from '../municipio-jurisdicion-camara';

export interface MaestroResponse {
  codigo: string;
  mensaje: string;
  selected: string;
  FirstFetchRecord: number;
  LastFetchRecord: number;
  RecordId: number;
  TotalFetchRecords: number;
  Municipios: Municipio[];
  MunicipiosJurisdicionCamara: MunicipioJurisdicionCamara[];
  paises: Pais[];
  tiposVia: TipoVia[];
  tipos_identificacion: TiposIdentificacion[];
}
