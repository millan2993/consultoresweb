import {Empresa} from '../empresa';

export interface ConsultarEmpresaResponse {
  codigo: string;
  mensaje: string;
  FirstFetchRecord: string;
  LastFetchRecord: string;
  RecordId: string;
  TotalFetchRecords: string;
  resultados: Empresa[];
}
