export interface Municipio {
  Pais: string;
  Dpto: string;
  Mpio: string;
  DescripcionMunicipio: string;
}
