import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, ReplaySubject, Subject} from 'rxjs';
import {BaseResponse} from '../interfaces/responses/base-response';
import {Empresa} from '../interfaces/empresa';
import {ConsultarEmpresaResponse} from '../interfaces/responses/consultar-empresa-response';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  empresa$: ReplaySubject<Empresa> = new ReplaySubject<Empresa>(1);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  get(identificacion: number): Observable<ConsultarEmpresaResponse> {
    const url = 'http://testing.clasificados.com.co/ccma/TramitesVirtuales/TramitesVirtuales.wsa/ConsultarEmpresaRegistro.html';

    return this.httpClient.post<any>(url, { identificacion }, this.httpOptions).pipe(
      map(response => response.response)
    );
  }

  save(empresa: Empresa): Observable<BaseResponse> {
    const url = 'http://testing.clasificados.com.co/ccma/PruebaTecnica/PruebaTecnica.wsa/Guardar.html';
    return this.httpClient.post<BaseResponse>(url, empresa, this.httpOptions);
  }

}
