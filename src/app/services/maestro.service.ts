import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MaestroResponse} from '../interfaces/responses/maestro-response';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

const maestroDefault = {
  Municipios: [],
  mensaje: '',
  TotalFetchRecords: 0,
  FirstFetchRecord: 0,
  LastFetchRecord: 0,
  RecordId: 0,
  codigo: '',
  MunicipiosJurisdicionCamara: [],
  paises: [],
  selected: '',
  tipos_identificacion: [],
  tiposVia: []
};

@Injectable({
  providedIn: 'root'
})
export class MaestroService {

  private url = 'http://testing.clasificados.com.co/ccma/TramitesVirtuales/TramitesVirtuales.wsa/ConsultarMaestros.html';

  private maestro: BehaviorSubject<MaestroResponse> = new BehaviorSubject<MaestroResponse>(maestroDefault);

  public maestro$ = this.maestro.asObservable();

  constructor(private http: HttpClient) {
    this.load()
      .subscribe(res => this.maestro.next(res));
  }

  load(): Observable<MaestroResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.get<any>(this.url, httpOptions)
      .pipe(
        map(res => res.response)
      );
  }

}
