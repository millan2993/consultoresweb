import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmpresaComponent} from './components/empresa/empresa.component';
import {AppComponent} from './app.component';
import {ConsultaComponent} from './components/consulta/consulta.component';

const routes: Routes = [
  { path: '', component: ConsultaComponent, pathMatch: 'full' },
  { path: 'empresa', component: EmpresaComponent },
  { path: '**', component: AppComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
