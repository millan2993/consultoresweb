import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {EmpresaService} from '../../services/empresa.service';
import {Empresa} from '../../interfaces/empresa';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MaestroService} from '../../services/maestro.service';
import {TiposIdentificacion} from '../../interfaces/tipos-identificacion';
import {Municipio} from '../../interfaces/municipio';
import {TipoVia} from '../../interfaces/tipo-via';
import {fromEvent, of, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.sass']
})
export class EmpresaComponent implements OnInit {

  // @ts-ignore
  empresa: Empresa;

  error = '';

  form: FormGroup = new FormGroup({});

  tiposVia: TipoVia[] = [];
  municipios: Municipio[] = [];
  naturalezas: TiposIdentificacion[] = [];

  search: Subject<Municipio[]> = new Subject<Municipio[]>();

  constructor(private empresaService: EmpresaService,
              private maestroService: MaestroService,
              private router: Router,
              private location: Location,
              private fb: FormBuilder) {

    this.empresaService.empresa$.subscribe(e => {
      this.empresa = e;
    });
  }

  ngOnInit(): void {
    if (!this.empresa) {
      this.router.navigate(['/']);
    }

    this.loadDataMaestra();
    this.buildForm();

    const searchBox = document.getElementById('Municipio');

    // @ts-ignore
    const typeahead = fromEvent(searchBox, 'input').pipe(
      map(e => (e.target as HTMLInputElement).value),
      filter(text => text.length > 1),
      debounceTime(10),
      distinctUntilChanged(),
      switchMap(searchTerm => of(
        this.municipios.filter(
          text => text.DescripcionMunicipio.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())))
      )
    );

    typeahead.subscribe(data => {
      this.search.next(data);
    });
  }

  private buildForm(): void {
    const direccionCompleta = this.empresa?.TipoVia + ' ' +
      this.empresa?.NumeroVia + ' ' +
      this.empresa?.NumeroCruce + ' ' +
      this.empresa?.Ubicacion;

    this.form = this.fb.group({
      Naturaleza: [this.empresa?.Naturaleza],
      Identificacion: [this.empresa?.Identificacion, [
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]],
      NombreEmpresa: [this.empresa?.NombreEmpresa],
      PrimerNombreUsuario: [this.empresa?.PrimerNombreUsuario],
      SegundoNombreUsuario: [this.empresa?.SegundoNombreUsuario],
      PrimerApellidoUsuario: [this.empresa?.PrimerNombreUsuario],
      SegundoApellidoUsuario: [this.empresa?.SegundoApellidoUsuario],
      EmailUsuario: [this.empresa?.EmailUsuario],
      Direccion: this.fb.group({
        TipoVia: [this.capitalize(this.empresa?.TipoVia), [ Validators.required ]],
        NumeroVia: [ this.empresa?.NumeroVia, [Validators.required, Validators.pattern('^[0-9]*$')] ],
        ApendiceVia: [''],
        NumeroCruce: [this.empresa?.NumeroCruce, [Validators.required, Validators.pattern('^[0-9]*$')]],
        ApendiceCruce: [''],
        Ubicacion: [this.empresa?.Ubicacion],
        DireccionCompleta: [direccionCompleta]
      }),
      Municipio: [this.empresa?.Municipio],
      Telefono: [this.empresa?.Telefono, [
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]],
      AutorizaEnvioSMS: [this.empresa?.AutorizaEnvioSMS],
      AutorizaEnvioEmail: [this.empresa?.AutorizaEnvioEmail]
    });
  }

  private loadDataMaestra(): void {
    this.maestroService.maestro$.subscribe(d => {
      this.naturalezas = d.tipos_identificacion;
      this.municipios = d.Municipios;
      this.tiposVia = d.tiposVia;
    });
  }

  onSubmit(): void {
    console.log(this.form.value);

    if (this.form.invalid) {
      this.error = 'Por favor revisa que los datos estén correctamente diligenciados.';
    }

    const temp: Empresa = {
      TipoIdentificacion: this.form.get('TipoIdentificacion')?.value,
      Identificacion: this.form.get('Identificacion')?.value,
      TipoIdentificacionUsuario: this.form.get('TipoIdentificacionUsuario')?.value,
      PrimerNombreUsuario: this.form.get('PrimerNombreUsuario')?.value,
      SegundoNombreUsuario: this.form.get('SegundoNombreUsuario')?.value,
      PrimerApellidoUsuario: this.form.get('PrimerApellidoUsuario')?.value,
      SegundoApellidoUsuario: this.form.get('SegundoApellidoUsuario')?.value,
      DireccionEmpresa: this.form.get('DireccionEmpresa')?.value,
      Pais: this.form.get('Pais')?.value,
      Departamento: this.form.get('Departamento')?.value,
      Municipio: this.form.get('Municipio')?.value,
      NombreEmpresa: this.form.get('NombreEmpresa')?.value,
      TipoVia: this.form.get('TipoVia')?.value,
      NumeroVia: this.form.get('NumeroVia')?.value,
      AdicionalVia: this.form.get('AdicionalVia')?.value,
      NumeroCruce: this.form.get('NumeroCruce')?.value,
      AdicionalCruce: this.form.get('AdicionalCruce')?.value,
      Ubicacion: this.form.get('Ubicacion')?.value,
      Telefono: this.form.get('Telefono')?.value,
      AutorizaEnvioEmail: this.form.get('AutorizaEnvioEmail')?.value,
      AutorizaEnvioSMS: this.form.get('AutorizaEnvioSMS')?.value,
      EmailNotificacionEmpresa: this.form.get('EmailUsuario')?.value,
      EmailUsuario: this.form.get('EmailUsuario')?.value
    };

    this.empresaService.save(temp).subscribe(r => {
      this.router.navigate(['/']);
    });
  }

  capitalize(str: string = ''): string {
    return str ? str.charAt(0).toUpperCase() + str.substr(1).toLowerCase() : '';
  }

  regresar(): void {
    this.location.back();
  }
}
