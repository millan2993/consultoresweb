import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EmpresaService} from '../../services/empresa.service';
import {Empresa} from '../../interfaces/empresa';
import {Router} from '@angular/router';


@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.sass']
})
export class ConsultaComponent {

  title = 'Inscripción al servicio';
  error = '';

  form: FormGroup = new FormGroup({
    identificacion: new FormControl('')
  });

  constructor(private empresaService: EmpresaService,
              private router: Router) { }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }

    const identificacion = this.form.get('identificacion')?.value;

    this.empresaService.get(identificacion).subscribe(response => {
      if (!response.resultados) {
        this.error = response.mensaje;
        return;
      }

      // devuelve 2 veces el mismo resultado. Algo que organizar en el backend
      const empresa: Empresa = response.resultados[0];
      this.empresaService.empresa$.next(empresa);
      this.router.navigate(['empresa']);
    });

  }

}
