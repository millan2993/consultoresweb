import {Component, OnInit} from '@angular/core';
import {MaestroService} from './services/maestro.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(private maestroService: MaestroService) { }

}
